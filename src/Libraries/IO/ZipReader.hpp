#pragma once

/* Application config. */
#include "emeraude_config.hpp"

#ifdef ZIP_ENABLED

/* STL inclusions. */
#include <filesystem>
#include <vector>
#include <string>

/* Third-party inclusions. */
#include <zip.h>

namespace Libraries::IO
{
	/**
	 * @brief Class to open a ZIP archive.
	 */
	class ZipReader final
	{
		public:

			static constexpr auto ClassId{"ZipReader"};

			/**
			 * @brief Constructs a zip archive reader.
			 * @param filepath A reference to a path.
			 */
			explicit ZipReader (std::filesystem::path filepath) noexcept;

			/**
			 * @brief Destructs the zip archive reader.
			 */
			~ZipReader ();

			/**
			 * @brief Changes the zip archive filepath.
			 * @param filepath A reference to a path.
			 * @return void
			 */
			void
			setFilepath (const std::filesystem::path & filepath) noexcept
			{
				this->close();

				m_filepath = filepath;
			}

			/**
			 * @brief Opens the archive.
			 * @return bool
			 */
			bool open () noexcept;

			/**
			 * @brief Closes the archive and release LibZip resources.
			 * @return void
			 */
			void close () noexcept;

			/**
			 * @brief Returns whether the archive is open.
			 * @return bool
			 */
			[[nodiscard]]
			bool
			isOpen () const noexcept
			{
				return m_zip != nullptr;
			}

			/**
			 * @brief Returns the filepath of the archive.
			 * @return const std::filesystem::path &
			 */
			const std::filesystem::path &
			filepath () const noexcept
			{
				return m_filepath;
			}

			/**
			 * @brief Returns the list of entry inside the archive.
			 * @return const std::vector< std::string > &
			 */
			[[nodiscard]]
			const std::vector< std::string > &
			entries () const noexcept
			{
				return m_entryNames;
			}

			/**
			 * @brief Extracts a specific entry from the ZIP archive to a buffer.
			 * @param entryName A reference to a string.
			 * @param buffer A reference to a vector.
			 * @return bool
			 */
			bool extract (const std::string & entryName, std::vector< char > & buffer) const noexcept;

			/**
			 * @brief Extracts a specific entry from the ZIP archive to a directory.
			 * @note The directory must exist.
			 * @param entryName A reference to a string.
			 * @param directory A reference to a path.
			 * @return bool
			 */
			bool extract (const std::string & entryName, const std::filesystem::path & directory) const noexcept;

			/**
			 * @brief Extracts all files present in the archive to a directory.
			 * @note The directory must exist.
			 * @param directory A reference to a path.
			 * @return bool
			 */
			bool extractAll (const std::filesystem::path & directory) const noexcept;

		private:

			/**
			 * @brief Opens the ZIP archive.
			 * @return bool
			 */
			[[nodiscard]]
			bool openArchive () noexcept;

			/**
			 * @brief Read the information about the archive to get entries.
			 * @return void
			 */
			void readArchive () noexcept;

			zip * m_zip{nullptr};
			std::filesystem::path m_filepath;
			std::vector< std::string > m_entryNames;
	};
}

#endif
