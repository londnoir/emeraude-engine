/*
 * src/Libraries/Time/Elapsed/ScopeRealTime.hpp
 * This file is part of Emeraude-Engine
 *
 * Copyright (C) 2010-2024 - "LondNoir" <londnoir@gmail.com>
 *
 * Emeraude-Engine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Emeraude-Engine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Emeraude-Engine; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 *
 * Complete project and additional information can be found at :
 * https://bitbucket.org/londnoir/emeraude-engine
 *
 * --- THIS IS AUTOMATICALLY GENERATED, DO NOT CHANGE ---
 */

#pragma once

/* STL inclusions. */
#include <chrono>
#include <cstdint>

namespace Libraries::Time::Elapsed
{
	/**
	 * @brief Gets the duration in wall clock time of a specific scope.
	 * @note The internal precision is set to nanoseconds.
	 * @tparam clockType The type of clock used. Default std::chrono::high_resolution_clock.
	 */
	template< typename clockType = std::chrono::high_resolution_clock >
	class ScopeRealTime final
	{
		public:

			/**
			 * @brief Construct a scope timer in nanoseconds.
			 * @param duration A reference where the duration will be accumulated in nanoseconds.
			 */
			explicit
			ScopeRealTime (uint64_t & duration) noexcept
				: m_duration(duration)
			{

			}

			/**
			 * @brief Copy constructor.
			 * @param copy A reference to the copied instance.
			 */
			ScopeRealTime (const ScopeRealTime & copy) noexcept = delete;

			/**
			 * @brief Move constructor.
			 * @param copy A reference to the copied instance.
			 */
			ScopeRealTime (ScopeRealTime && copy) noexcept = delete;

			/**
			 * @brief Copy assignment.
			 * @param copy A reference to the copied instance.
			 */
			ScopeRealTime & operator= (const ScopeRealTime & copy) noexcept = delete;

			/**
			 * @brief Move assignment.
			 * @param copy A reference to the copied instance.
			 */
			ScopeRealTime & operator= (ScopeRealTime && copy) noexcept = delete;

			/**
			 * @brief Destructs the scope timer
			 */
			~ScopeRealTime ()
			{
				m_duration += std::chrono::duration_cast< std::chrono::nanoseconds >(clockType::now() - m_startTime).count();
			}

		private:

			uint64_t & m_duration;
			std::chrono::time_point< clockType > m_startTime{clockType::now()};
	};
}
