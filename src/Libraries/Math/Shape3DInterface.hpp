/*
 * src/Libraries/Math/Shape3DInterface.hpp
 * This file is part of Emeraude-Engine
 *
 * Copyright (C) 2010-2024 - "LondNoir" <londnoir@gmail.com>
 *
 * Emeraude-Engine is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * Emeraude-Engine is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Emeraude-Engine; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301  USA
 *
 * Complete project and additional information can be found at :
 * https://bitbucket.org/londnoir/emeraude-engine
 *
 * --- THIS IS AUTOMATICALLY GENERATED, DO NOT CHANGE ---
 */

#pragma once

/* STL inclusions. */
#include <type_traits>

/* Local inclusions for inheritances. */
#include "ShapeInterface.hpp"

namespace Libraries::Math
{
	/**
	 * @brief The abstract 3D shape class.
	 * @tparam data_t The type used for geometric distance and dimensions. Default float.
	 * @extends Libraries::Math::ShapeInterface This is a geometric shape.
	 */
	template< typename data_t = float >
	requires (std::is_arithmetic_v< data_t >)
	class Shape3DInterface : public ShapeInterface< data_t >
	{
		public:

			/**
			 * @brief Returns the area of the 3D shape.
			 * @return data_t
			 */
			[[nodiscard]]
			virtual data_t getVolume () const noexcept = 0;

		protected:

			/**
			 * @brief Constructs an abstract 3D shape.
			 */
			Shape3DInterface () noexcept = default;
	};
}
