if ( EMERAUDE_USE_SYSTEM_LIBS )

	message("Enabling LibZib library from system ...")

	find_package(PkgConfig REQUIRED)

	pkg_check_modules(LibZib REQUIRED libzip)

	target_include_directories(${PROJECT_NAME} PUBLIC ${LIBZIP_INCLUDE_DIRS})
	target_link_directories(${PROJECT_NAME} PUBLIC ${LIBZIP_LIBRARY_DIRS})

else ()

	message("Enabling LibZib library from local source ...")

	if ( NOT EXISTS ${LOCAL_LIB_DIR}/include/zip.h )
		set(LIBZIP_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/libzip)
		set(LIBZIP_BUILD_DIR ${TEMPORARY_BUILD_DIR}/libzip-${CMAKE_BUILD_TYPE})

		#option(ENABLE_COMMONCRYPTO "Enable use of CommonCrypto" ON)
		#option(ENABLE_GNUTLS "Enable use of GnuTLS" ON)
		#option(ENABLE_MBEDTLS "Enable use of mbed TLS" ON)
		#option(ENABLE_OPENSSL "Enable use of OpenSSL" ON)
		#option(ENABLE_WINDOWS_CRYPTO "Enable use of Windows cryptography libraries" ON)
		#option(ENABLE_BZIP2 "Enable use of BZip2" ON)
		#option(ENABLE_LZMA "Enable use of LZMA" ON)
		#option(ENABLE_ZSTD "Enable use of Zstandard" ON)
		#option(ENABLE_FDOPEN "Enable zip_fdopen, which is not allowed in Microsoft CRT secure libraries" ON)

		if ( UNIX AND NOT APPLE )
			execute_process(
				COMMAND ${CMAKE_COMMAND}
				-S ${LIBZIP_SOURCE_DIR}
				-B ${LIBZIP_BUILD_DIR}
				-DCMAKE_INSTALL_PREFIX=${LOCAL_LIB_DIR}
				-DCMAKE_C_FLAGS=-fPIC
				-DBUILD_TOOLS=Off
				-DBUILD_REGRESS=Off
				-DBUILD_OSSFUZZ=Off
				-DBUILD_EXAMPLES=Off
				-DBUILD_DOC=Off
				-DBUILD_SHARED_LIBS=Off
				${CMAKE_GEN_EXTRA_FLAGS}
				COMMAND_ERROR_IS_FATAL ANY
			)
		elseif ( APPLE )
			execute_process(
				COMMAND ${CMAKE_COMMAND}
				-S ${LIBZIP_SOURCE_DIR}
				-B ${LIBZIP_BUILD_DIR}
				-DCMAKE_INSTALL_PREFIX=${LOCAL_LIB_DIR}
				-DCMAKE_C_FLAGS="-mmacosx-version-min=${CMAKE_OSX_DEPLOYMENT_TARGET} -fPIC"
				-DBUILD_TOOLS=Off
				-DBUILD_REGRESS=Off
				-DBUILD_OSSFUZZ=Off
				-DBUILD_EXAMPLES=Off
				-DBUILD_DOC=Off
				-DBUILD_SHARED_LIBS=Off
				${CMAKE_GEN_EXTRA_FLAGS}
				COMMAND_ERROR_IS_FATAL ANY
			)
		elseif ( MSVC )
			execute_process(
				COMMAND ${CMAKE_COMMAND}
				-S ${LIBZIP_SOURCE_DIR}
				-B ${LIBZIP_BUILD_DIR}
				-DCMAKE_INSTALL_PREFIX=${LOCAL_LIB_DIR}
				-DCMAKE_MSVC_RUNTIME_LIBRARY=MultiThreaded$<$<CONFIG:Debug>:Debug>
				-DBUILD_TOOLS=Off
				-DBUILD_REGRESS=Off
				-DBUILD_OSSFUZZ=Off
				-DBUILD_EXAMPLES=Off
				-DBUILD_DOC=Off
				-DBUILD_SHARED_LIBS=Off
				${CMAKE_GEN_EXTRA_FLAGS}
				COMMAND_ERROR_IS_FATAL ANY
			)
		else ()
			message(FATAL_ERROR "Unknown platform !")
		endif ()

		execute_process(COMMAND ${CMAKE_COMMAND} --build ${LIBZIP_BUILD_DIR} --config ${CMAKE_BUILD_TYPE} --parallel ${CMAKE_BUILD_PARALLEL_LEVEL} COMMAND_ERROR_IS_FATAL ANY)

		execute_process(COMMAND ${CMAKE_COMMAND} --install ${LIBZIP_BUILD_DIR} --config ${CMAKE_BUILD_TYPE} COMMAND_ERROR_IS_FATAL ANY)
	endif ()

	set(LIBZIP_VERSION "1.11.3git")
	set(LIBZIP_INCLUDE_DIRS ${LOCAL_LIB_DIR}/include)
	set(LIBZIP_LIBRARY_DIRS ${LOCAL_LIB_DIR}/lib)
	set(LIBZIP_LIBRARIES ${LOCAL_LIB_DIR}/lib/libzip.a)
endif ()

message("LibZip ${LIBZIP_VERSION} library enabled !")
message(" - Headers : ${LIBZIP_INCLUDE_DIRS}")
message(" - Libraries : ${LIBZIP_LIBRARY_DIRS}")
message(" - Binary : ${LIBZIP_LIBRARIES}")

target_link_libraries(${PROJECT_NAME} PUBLIC ${LIBZIP_LIBRARIES})

set(ZIP_ENABLED On) # Complete the "libraries_config.hpp" file
