message("Enabling cpu_features library from local source ...")

if ( NOT EXISTS ${LOCAL_LIB_DIR}/include/cpu_features )
	set(CPUFEATURES_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}/dependencies/cpu_features)
	set(CPUFEATURES_BUILD_DIR ${TEMPORARY_BUILD_DIR}/cpu_features-${CMAKE_BUILD_TYPE})

	if ( UNIX AND NOT APPLE )
		execute_process(
			COMMAND ${CMAKE_COMMAND}
			-S ${CPUFEATURES_SOURCE_DIR}
			-B ${CPUFEATURES_BUILD_DIR}
			-DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
			-DCMAKE_INSTALL_PREFIX=${LOCAL_LIB_DIR}
			-DCMAKE_C_FLAGS=-fPIC
			-DBUILD_TESTING=Off
			-DBUILD_EXECUTABLE=Off
			-DBUILD_SHARED_LIBS=Off
			-DENABLE_INSTALL=On
			${CMAKE_GEN_EXTRA_FLAGS}
			COMMAND_ERROR_IS_FATAL ANY
		)
	elseif ( APPLE )
		execute_process(
			COMMAND ${CMAKE_COMMAND}
			-S ${CPUFEATURES_SOURCE_DIR}
			-B ${CPUFEATURES_BUILD_DIR}
			-DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
			-DCMAKE_INSTALL_PREFIX=${LOCAL_LIB_DIR}
			-DCMAKE_C_FLAGS="-mmacosx-version-min=${CMAKE_OSX_DEPLOYMENT_TARGET} -fPIC"
			-DBUILD_TESTING=Off
			-DBUILD_EXECUTABLE=Off
			-DBUILD_SHARED_LIBS=Off
			-DENABLE_INSTALL=On
			${CMAKE_GEN_EXTRA_FLAGS}
			COMMAND_ERROR_IS_FATAL ANY
		)
	elseif ( MSVC )
		execute_process(
			COMMAND ${CMAKE_COMMAND}
			-S ${CPUFEATURES_SOURCE_DIR}
			-B ${CPUFEATURES_BUILD_DIR}
			-DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
			-DCMAKE_INSTALL_PREFIX=${LOCAL_LIB_DIR}
			-DCMAKE_MSVC_RUNTIME_LIBRARY=MultiThreaded$<$<CONFIG:Debug>:Debug>
			-DBUILD_TESTING=Off
			-DBUILD_EXECUTABLE=Off
			-DBUILD_SHARED_LIBS=Off
			-DENABLE_INSTALL=On
			${CMAKE_GEN_EXTRA_FLAGS}
			COMMAND_ERROR_IS_FATAL ANY
		)
	else ()
		message(FATAL_ERROR "Unknown platform !")
	endif ()

	execute_process(COMMAND ${CMAKE_COMMAND} --build ${CPUFEATURES_BUILD_DIR} --config ${CMAKE_BUILD_TYPE} --parallel ${CMAKE_BUILD_PARALLEL_LEVEL} COMMAND_ERROR_IS_FATAL ANY)

	execute_process(COMMAND ${CMAKE_COMMAND} --install ${CPUFEATURES_BUILD_DIR} --config ${CMAKE_BUILD_TYPE} COMMAND_ERROR_IS_FATAL ANY)
endif ()

set(CPUFEATURES_VERSION "0.9.0git")
set(CPUFEATURES_INCLUDE_DIRS ${LOCAL_LIB_DIR}/include)
set(CPUFEATURES_LIBRARY_DIRS ${LOCAL_LIB_DIR}/lib)
set(CPUFEATURES_LIBRARIES cpu_features)

message("cpu_features ${CPUFEATURES_VERSION} library enabled !")
message(" - Headers : ${CPUFEATURES_INCLUDE_DIRS}")
message(" - Libraries : ${CPUFEATURES_LIBRARY_DIRS}")
message(" - Binary : ${CPUFEATURES_LIBRARIES}")

target_link_libraries(${PROJECT_NAME} PUBLIC ${CPUFEATURES_LIBRARIES})

set(CPUFEATURES_ENABLED On) # Complete the "libraries_config.hpp" file
